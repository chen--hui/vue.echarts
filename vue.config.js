const path = require('path')
const resolve = dir => path.join(__dirname, dir)
const BASE_URL = process.env.NODE_ENV === 'prodution' ? './' : './'
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const isDev = process.env.NODE_ENV === 'development'

// 引入等比适配插件
const px2rem = require('postcss-px2rem')

// 配置基本大小
const postcss = px2rem({
  // 基准大小 baseSize，需要和rem.js中相同
  remUnit: 384
})

const assetsCDN = {
  externals: {
    vue: 'Vue',
    vuex: 'Vuex',
    'vue-router': 'VueRouter',
    'element-ui': 'ELEMENT',
    axios: 'axios'
  },
  css: [],
  js: [
    // `${ isDev ? '//cdn.bootcss.com/vue/2.6.11/vue.js' : '//cdn.bootcss.com/vue/2.6.11/vue.min.js'}`,
    // '//cdn.bootcss.com/vue-router/3.2.0/vue-router.min.js',
    // '//cdn.bootcss.com/vuex/3.0.1/vuex.min.js',
    // '//unpkg.com/axios/dist/axios.min.js',
    // '//unpkg.com/element-ui@2.13.2/lib/index.js'
  ]
}

module.exports = {
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          postcss
        ]
      }
    }
  },
  lintOnSave: false,
  publicPath: BASE_URL,
  chainWebpack: config => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('_c', resolve('src/components'))
  },
  configureWebpack: (config) => {
    // config.externals = assetsCDN.externals,
    config.module.rules.push({
      test: /\.swf$/,
      use: [{
        loader: 'file-loader'
      }]
    })
    config.plugins.forEach(e => {
      if (e instanceof HtmlWebpackPlugin) {
        e.options.title = '南阳智慧大屏展示'
        e.options.cdn = assetsCDN
      }
    })
  },
  // 打包时不生成.map文件（减少打包体积、加快打包速度）
  productionSourceMap: false,
  // 打包后文件夹名称
  // outputDir: 'adminback',
  devServer: {
    port: 8000,
    proxy: {
      '/bpi': {
        target: `${process.env.VUE_APP_API_BASE_PROXY_URL}`,
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          '^/bpi': '' // rewrite path
        }
      }
    }
  }

}
