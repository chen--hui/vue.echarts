import moment from 'moment'
export const filterTimeToMinutes = function(v, flag ='normal') {
  if(!v) return ''
  if (!isNaN(Number(v))) {
    v = Number(v)
  }
  const time = moment(v)
  let rs = time.format('MM-DD HH:mm')
  if(flag == 'min') {
    rs = time.format('HH:mm')
  }
  if(flag == 'second') {
    rs =  time.format('MM-DD HH:mm:ss')
  }
  return rs
}

export const fontSize = (res) => {
  let docEl = document.documentElement,
    clientWidth = window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;
  if (!clientWidth) return;
  let fontSize = (clientWidth / 3840);
  return res*fontSize;
}
