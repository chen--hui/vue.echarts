import Vue from 'vue'
import Vuex from 'vuex'
import states from './states'
import * as getters from './getters'
import actions from './actions'
import mutations from './mutations'
// import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  state: states,
  mutations: mutations,
  getters: getters,
  actions: actions,
  modules: {
  }
})

