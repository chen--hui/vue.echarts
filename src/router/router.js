const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: () => import('@/views/dashboard/index.vue'),
    meta: {
      title: '数据概况'
    }
  },
  {
    path: '/alarmCenter',
    name: 'alarmCenter',
    component: () => import('@/views/alarmCenter/index.vue'),
    meta: {
      title: '报警中心'
    }
  }
]

export default routes
