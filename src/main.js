import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
// 预览图片插件
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
import 'element-ui/lib/theme-chalk/index.css'
import scroll from 'vue-seamless-scroll'
import 'amfe-flexible'
import echarts from 'echarts'
import filters from '@/utils/filters'
import videoBox from './components/videoBox/index'
import AmapVueConfig from '@amap/amap-vue/lib/config'
// import AmapVue from '@amap/amap-vue'
import '@/utils/rem'
import '@/assets/css/global.scss'
import API from '@/api'
// AmapVue.config.version = '2.0' // 默认2.0，这里可以不修改
// AmapVue.config.key = 'ce2cc39d87a2be05467103387349e38a'
// AmapVue.config.plugins = [
//   'AMap.ToolBar',
//   'AMap.MoveAnimation'
//   // 在此配置你需要预加载的插件，如果不配置，在使用到的时候会自动异步加载
// ]
// Vue.use(AmapVue)
AmapVueConfig.key = 'ce2cc39d87a2be05467103387349e38a'
AmapVueConfig.plugins = [
  'AMap.ToolBar',
  'AMap.MoveAnimation'
  // 在此配置你需要预加载的插件，如果不配置，在使用到的时候会自动异步加载
]
Vue.use(videoBox)
Vue.use(scroll)

Vue.use(Viewer, {
  defaultOptions: {
    zIndex: 9999,
    toolbar: false
  }
})
Vue.prototype.$API = API
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.use(ElementUI)

// 注册全局过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// const allowList = ['login', 'register', 'registerResult'] // no redirect allowList

// router.beforeEach((to, from, next) => {
//   if (store?.state?.storages?.user?.info?.bigToken) {
//     // 没有路由的情况下  我们动态生成一个路由
//     next()
//   } else {
//     if (allowList.includes(to.name)) {
//       // 在免登录名单，直接进入
//       next()
//     } else {
//       // 暂时移除redirect
//       // next({ path: loginRoutePath, query: { redirect: to.fullPath } })
//       store.dispatch('toLogin', 0)
//     }
//   }
// });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
