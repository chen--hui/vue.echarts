// export const socketURL = 'ws://129.28.12.132:8086/websocket/1' // 正式
// export const accessAndReportSocket = "ws://129.28.207.245:8092/websocket"
export const socketURL = `${process.env.VUE_APP_API_SOCKET_URL}/websocket/1`
export const accessAndReportSocket = `${process.env.VUE_APP_API_ACCESS_SOCKET_URL}/websocket`
export const noticeURL = 'http://28lb009592.wicp.vip:8141' // 广播系统
export const messageURL = 'http://s37738z782.qicp.vip:8142' // 'http://47.101.36.40:8090' // 信息发布系统
