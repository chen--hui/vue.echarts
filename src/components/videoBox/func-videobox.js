import videoBox from './videoBox.vue'

export default {
  extends: videoBox,
  computed: {
    style () {
      const { initPos: { x, y } } = this
      const app = document.getElementById('app')
      const aWidth = app.clientWidth
      const aHeight = app.clientHeight
      let left = (x - this.$options.filters['fontSize'](750))
      let top = (y - this.$options.filters['fontSize'](450))
      if (left < 0 || !left) {
        left = 0
      }
      if (top < 0 || !top) {
        top = 0
      }
      if (left > (aWidth - this.$options.filters['fontSize'](750))) {
        left = aWidth - this.$options.filters['fontSize'](750)
      }
      if (top > (aHeight - this.$options.filters['fontSize'](450))) {
        top = aHeight - this.$options.filters['fontSize'](450)
      }
      return {
        position: 'absolute',
        zIndex: this.zindex,
        left: this.left ? this.left : `${left}px` || '',
        top: this.top ? this.top : `${top}px` || '',
        transition: this.transition
      }
    }
  },
  mounted () {
    this.createTimer()
  },
  beforeDestroy () {
    this.clearTimer()
  },
  methods: {
    createTimer () {
      // if (this.autoClose) {
      //   this.timer = setTimeout(() => {
      //     this.isVisible = false
      //   }, this.autoClose)
      // }
    },
    mouseDown (e) {
      this.zindex = this.handleSeed()
      const odiv = document.getElementsByClassName('video_box')[0] // 获取目标元素
      this.transition = ''
      // 算出鼠标相对元素的位置
      const disX = e.clientX - odiv.offsetLeft
      const disY = e.clientY - odiv.offsetTop
      // 鼠标按下并移动的事件
      document.onmousemove = (e) => {
        // 用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
        const left = e.clientX - disX
        const top = e.clientY - disY
        // 绑定元素位置到positionX和positionY上面
        // this.y = top;
        // this.x = left;
        // 移动当前元素
        this.left = left + 'px'
        this.top = top + 'px'
      }
      document.onmouseup = (e) => {
        this.transition = 'all .2s'
        document.onmousemove = null
        document.onmouseup = null
      }
    },
    handleSeed () {
      let seed = sessionStorage.getItem("seed")
      if (!seed) {
        seed = 1000
      } else {
        seed = Number.parseInt(seed) + 1
      }
      sessionStorage.setItem("seed", seed)
      return seed
    },
    clearTimer () {
      if (this.timer) {
        clearTimeout(this.timer)
      }
    },
    afterEnter () {
      this.zindex = this.zindex
    }
  },
  props: {
    initPos: {
      type: Object,
      default: () => ({
        x: 0,
        y: 0
      })
    }
  },
  data () {
    return {
      zindex: 0,
      height: 0,
      z: 0,
      left: 0,
      top: 0,
      transition: 'all .2s'
    }
  }
}
