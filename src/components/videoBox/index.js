import VideoBox from './videoBox.vue'
import videobox from './function'
export default (Vue) => {
  Vue.component(VideoBox.name, VideoBox)
  Vue.prototype.$videoBox = videobox
}
