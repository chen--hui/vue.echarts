export default {
  GJ_LIST: '/alarmInfo/getList',
  GJ_STATUS: '/alarmInfo/updateStatus',
  SEND_MSG: '/addressBook/sendSMSMsg',
  VIDEO_LIST: '/deviceInfo/getCameraVideoList', // 所有视频
  GETVIDEO: '/deviceInfo/getCameraVideo', // 单设备视频
  VIDEO_BACK: '/deviceInfo/getCameraPlayback'// 视频回放
}
