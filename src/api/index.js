import Axios from '@/utils/request'
import { isString, isFunction } from 'lodash'
import alamCenterApi from './alamCenter'
import warnCenter from './dashboard/warnCenter'
import head from './head'
import deviceApi from './device'

const axios = new Axios()

export {
  alamCenterApi,
  head,
  deviceApi,
  warnCenter
}

export default async function (apiConfig, data = {}, configs = {
  method: 'post',
  noMsg: false, // 不显示错误信息
  neglectErr: false // 不进入错误日志  此处忽略的是接口code返回的错误状态  而不是http请求的错误
}) {
  if (isFunction(apiConfig)) {
    return apiConfig(data, configs)
  }

  if (isString(apiConfig)) {
    return axios.request({
      url: apiConfig,
      method: configs.method ?? 'post',
      headers: {
        ...configs
      },
      data
    })
  }

  return axios.request({
    method: 'post',
    data,
    ...apiConfig,
    headers: {
      ...configs,
      ...apiConfig.headers || {}
    }
  })
}

// 调用下载文件接口的方法  下载之后请自行使用util包里面的exportFile导出文件到本地
export const DOWNLOAD_API = async (apiConfig, data = {}) => {
  return axios.request({
    url: apiConfig,
    method: 'post',
    responseType: 'blob',
    headers: {
      neglectErr: true
    },
    data
  })
}
