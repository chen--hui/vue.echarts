export default {
  LIST: '/deviceInfo/getDeviceListStatus',
  GET_CAMERA_URL: '/deviceInfo/getCameraVideo'
}